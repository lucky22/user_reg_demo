package com.example.sampleuserapp.appuser;

    public class AppUserResponse{
        private boolean status;
        private String token;
        private AppUser user;

        public AppUserResponse(boolean status, String token, AppUser user) {
            this.status = status;
            this.token = token;
            this.user = user;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public AppUser getUser() {
            return user;
        }

        public void setUser(AppUser user) {
            this.user = user;
        }
    }
