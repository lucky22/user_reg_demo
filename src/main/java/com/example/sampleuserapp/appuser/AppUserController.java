package com.example.sampleuserapp.appuser;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class AppUserController {

    private AppUserRepository appUserRepository;

    public AppUserController(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @GetMapping("/")
    public String welcome(){
        return "Your are welcome";
    }

    @GetMapping("/api/v1/users")
    public List<AppUser> getUsers(){
        return appUserRepository.findAll();
    }

}
