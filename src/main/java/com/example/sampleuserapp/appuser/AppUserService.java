package com.example.sampleuserapp.appuser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class AppUserService implements UserDetailsService {

    @Autowired
    private final AppUserRepository appUserRepository;
    @Autowired
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public AppUserService(
                          AppUserRepository appUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {

        this.appUserRepository = appUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return appUserRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with email %s not found", email)));
    }

    public AppUserResponse signUpUser(AppUser appUser){
        boolean hasUser = appUserRepository.findByEmail(appUser.getEmail()).isPresent();
        if(hasUser){
            throw new IllegalStateException("Email already exist");
        }
        String encodedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
        appUser.setPassword(encodedPassword);
        appUser.setEnabled(true);
        appUserRepository.save(appUser);

        String token = UUID.randomUUID().toString();

        return new AppUserResponse(true, token, appUser);
    }

    public AppUserResponse updatePassword(String email, String password){
        Optional<AppUser> user = appUserRepository.findByEmail(email);
        boolean hasUser = user.isPresent();
        if(!hasUser){
            throw new IllegalStateException("User does not exist");
        }

        String encodedPassword = bCryptPasswordEncoder.encode(password);
        user.get().setPassword(encodedPassword);
        appUserRepository.save(user.get());
        String token = UUID.randomUUID().toString();

        return new AppUserResponse(true, token, user.get());
    }

    public AppUserResponse updateProfile(String email, String address){
        Optional<AppUser> user = appUserRepository.findByEmail(email);
        boolean hasUser = user.isPresent();
        if(!hasUser){
            throw new IllegalStateException("User does not exist");
        }
        user.get().setAddress(address);
        appUserRepository.save(user.get());
        String token = UUID.randomUUID().toString();
        return new AppUserResponse(true, token, user.get());
    }

    public AppUserResponse login(String email, String password){
        Optional<AppUser> user = appUserRepository.findByEmail(email);
        boolean hasUser = user.isPresent();
        if(!hasUser){
            throw new IllegalStateException("User does not exist");
        }

//        String encodedPassword = bCryptPasswordEncoder.encode(password);
//        user.get().setPassword(encodedPassword);
//        appUserRepository.save(user.get());
        boolean isPasswordCorrect = bCryptPasswordEncoder.matches(password, user.get().getPassword());

        if(!isPasswordCorrect){
            throw new IllegalStateException("Bad credentials");
        }
        String token = UUID.randomUUID().toString();

        return new AppUserResponse(true, token, user.get());
    }

}
