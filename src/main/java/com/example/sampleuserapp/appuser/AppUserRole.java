package com.example.sampleuserapp.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
