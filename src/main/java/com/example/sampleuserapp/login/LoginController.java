package com.example.sampleuserapp.login;

import com.example.sampleuserapp.appuser.AppUserResponse;
import com.example.sampleuserapp.forgotpassword.ForgotPasswordRequest;
import com.example.sampleuserapp.forgotpassword.ForgotPasswordService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/api/v1/login")
    public AppUserResponse loginUser(@RequestBody LoginRequest request){
        return loginService.login(request);
    }

}
