package com.example.sampleuserapp.login;

import com.example.sampleuserapp.appuser.AppUserResponse;
import com.example.sampleuserapp.appuser.AppUserService;
import com.example.sampleuserapp.profile.ProfileRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LoginService {

    @Autowired
    private final AppUserService appUserService;

    public LoginService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    public AppUserResponse login(LoginRequest request) {
        return appUserService.login(
                request.getEmail(),
                request.getPassword()
        );
    }
}