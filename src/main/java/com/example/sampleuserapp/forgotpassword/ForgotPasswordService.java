package com.example.sampleuserapp.forgotpassword;

import com.example.sampleuserapp.appuser.AppUserResponse;
import com.example.sampleuserapp.appuser.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForgotPasswordService {

    @Autowired
    private final AppUserService appUserService;

    public ForgotPasswordService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    public AppUserResponse forgotPassword(ForgotPasswordRequest request) {
        return appUserService.updatePassword(
                request.getEmail(),
                request.getPassword()
        );
    }


}
