package com.example.sampleuserapp.forgotpassword;

public class ForgotPasswordRequest {
    private final String email;
    private final String password;

    public ForgotPasswordRequest(String email,
                                 String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "LoginRequest{" +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
