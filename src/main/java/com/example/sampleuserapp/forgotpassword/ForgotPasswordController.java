package com.example.sampleuserapp.forgotpassword;


import com.example.sampleuserapp.appuser.AppUserResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class ForgotPasswordController {

    @Autowired
    private ForgotPasswordService forgotPasswordService;

    @PutMapping("/api/v1/updatePassword")
    public AppUserResponse updatePassword(@RequestBody ForgotPasswordRequest request){
        return forgotPasswordService.forgotPassword(request);
    }
}
