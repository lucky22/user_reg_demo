package com.example.sampleuserapp.registration;

import com.example.sampleuserapp.appuser.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    @Autowired
    private final EmailValidator emailValidator;
    @Autowired
    private final AppUserService appUserService;

    public RegistrationService(EmailValidator emailValidator,
                               AppUserService appUserService) {
        this.emailValidator = emailValidator;
        this.appUserService = appUserService;
    }

    public AppUserResponse register(RegistrationRequest request) {
        boolean isEmailValid =  emailValidator.test(request.getEmail());
        if (!isEmailValid){
            throw new IllegalStateException("Email not valid");
        }
        return appUserService.signUpUser(new AppUser(
            request.getFirstName(),
                request.getLastName(),
                request.getEmail(),
                request.getPassword()
        ));
    }
}
