package com.example.sampleuserapp.registration;


import com.example.sampleuserapp.appuser.AppUserResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @PostMapping("/api/v1/signup")
    public AppUserResponse register(@RequestBody RegistrationRequest request){
        return registrationService.register(request);
    }

}
