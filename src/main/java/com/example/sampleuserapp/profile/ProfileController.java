package com.example.sampleuserapp.profile;

import com.example.sampleuserapp.appuser.AppUserResponse;
import com.example.sampleuserapp.registration.RegistrationRequest;
import com.example.sampleuserapp.registration.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @PutMapping("/api/v1/updateProfile")
    public AppUserResponse updateProfile(@RequestBody ProfileRequest request){
        return profileService.updateUserProfile(request);
    }

}