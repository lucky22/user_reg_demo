package com.example.sampleuserapp.profile;

import com.example.sampleuserapp.appuser.AppUserResponse;
import com.example.sampleuserapp.appuser.AppUserRole;
import com.example.sampleuserapp.appuser.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {

    @Autowired
    private final AppUserService appUserService;

    public ProfileService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    public AppUserResponse updateUserProfile(ProfileRequest request) {
        return appUserService.updateProfile(
                request.getEmail(),
                request.getAddress()
        );
    }
}
