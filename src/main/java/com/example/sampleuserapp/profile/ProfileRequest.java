package com.example.sampleuserapp.profile;

import com.example.sampleuserapp.appuser.AppUserRole;

public class ProfileRequest {
    private final String email;
    private final String address;

    public ProfileRequest(String email, String address) {
        this.email = email;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }
}
