package com.example.sampleuserapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleUserAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleUserAppApplication.class, args);
	}

}
